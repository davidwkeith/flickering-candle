require 'hue'

HUE_LIGHT_ID = 5
CANDLE_COLOR_RANGE = [*2000..2200] # Kelvin, 2000K is lowest Hue can do
CANDLE_BRIGHTNESS_RANGE = [*30..75]

client = Hue::Client.new
candle = client.lights[HUE_LIGHT_ID]

candle.on!

loop do
  begin
    @delay = (rand * 0.2200) + 0.08
    candle.set_state({
        :color_temperature => CANDLE_COLOR_RANGE.sample,
        :brightness => CANDLE_BRIGHTNESS_RANGE.sample
      }, 1)
  rescue
    puts 'rate limit hit, increasing delay'
    @delay = @delay * 1.5
  end
  sleep @delay
end
